---
title: "KDE e.V. Community Partnership Program"
menu:
  main:
    parent: activities
    name: Community Partnership Program
    weight: 4
---

## Preamble

The goal of KDE e.V. is to support free software development, promotion, and education in general, and KDE specifically. We perceive ourselves to be part of the greater free software movement, embedded in the broad context of free culture. While we are dedicated to supporting the KDE community, we also work together with other communities, organizations and individuals to further our common goals. The Community Partnership Program provides a framework for cooperation.

## Goal of the program

<p>The goal of the Community Partnership Program is to offer a communication channel, foster cooperation, and provide for common interests. The Program is meant to facilitate joint activities and exchange of experience. While the Program involves some formality for providing a serious foundation, it is intended to be a light-weight framework that offers opportunities, but doesn't come with a lot of formal obligations.</p>

<h2>Relationship to other programs</h2>

<p>The Community Partnership Program is meant to provide a formal base for situations where we can collaborate and help one another. It is not meant to replace any other programs of KDE e.V. or the KDE community, including normal or supporting memberships, or other agreements KDE e.V. might do with other organizations or communities.</p>

<h2>Joining the program</h2>

<p>All organizations or communities sharing the general goals of KDE e.V. are eligible to become members of the Community Partnership Program. Community Partners should have some affinity with KDE, by being active in similar or complementary areas, sharing general goals and a dedication to freedom.</p>

<p>Partners are expected to act in accordance with KDE's code of conduct when interacting with the KDE community.</p>

<p>The Board of KDE e.V. decides about accepting members to the Community Partnership Program. </p>

<p>Members of the Community Partnership Program are listed on the KDE e.V. web site.</p>
<!--<p>Members of the Community Partnership Program are <a href="/community-partners/">listed on the KDE e.V. web site</a>.</p>-->

<h2>Partner contact</h2>

<p>Each member appoints a contact person of their choosing who represents them within the Community Partnership Program. The member notifies the Board of KDE e.V. (kde-ev-board@kde.org) of the initial choice of the contact person and any subsequent changes.</p>

<h2>Benefits</h2>

<p>The primary benefit of membership in the Community Partnership Program is to give visibility to the alignment of interests and express the intention to cooperate as communities.</p>

<h3>Financial proxy</h3>

<p>A member can request to become part of the Financial Proxy Program. For members accepted to the Financial Proxy Program, KDE e.V. acts as financial proxy. KDE e.V. can receive funds on behalf of the partner community for spending them on causes of the partner community.</p>

<p>The Board of KDE e.V. decides about acceptance of members to the Financial Proxy Program.</p>

<h4>Receiving funds</h4>

<p>The partner community can direct their constituency to send money to KDE e.V. where it is allocated for use for the partner. This is expected to be donations mostly. For any other type of income, the Board of KDE e.V. and the partner community together will discuss and decide how to handle the funds.</p>

<p>Funds which are received on behalf of a partner are specifically allocated for the partner community use.</p>

<p>KDE e.V. retains 3% of the received funds to cover administration and other costs related to the program.</p>

<h4>Allocating partner funds</h4>

<p>Funds received on behalf of a partner are spent on activities of the partner community. All allocations of funds have to be in line with the goals and constraints of KDE e.V. Usually supportable activities are travel of community members, promotion material, or community infrastructure.</p>

<p>A committee consisting of two members of the partner community and one member of the Board of KDE e.V. decide and approve all fund allocations. The committee decides by majority vote. The KDE e.V. Board member has a veto right if the proposed allocation isn't compatible with KDE e.V.s regulations or policies.</p>

<p>The partner community contact person appoints the necessary committee members for the partner community. The Board of KDE e.V. appoints the committee member for KDE e.V.</p>

<p>Requests for allocation of funds should be made to the committee at least two weeks before the funds are needed in order to allow for enough time to discuss and decide on requests.</p>

<p>Funds may be used to pay invoices on behalf of the partner or to reimburse expenses incurred by the members of the partner community. Original invoices or receipts have to be provided to KDE e.V. for accounting purposes.</p>

<h4>Accounting</h4>

<p>KDE e.V. will create quarterly account statements stating income and expenses of the partner funds and will make them available to the partner in electronic form. KDE e.V. will keep statements for at least one year.</p>

<h2>Leaving the program</h2>

<p>Either side, KDE e.V. or the partner, can decide to discontinue the partnership according to this program at any time without any restrictions. Discontinuation of the program has to be announced via email to the Board of KDE e.V. and the contact person of the partner at least four weeks in advance of becoming effective.</p>

<p>If the partner is a member of the Financial Proxy Program, any remaining funds that are allocated to the partner must be spent in accordance with the Financial Proxy Program rules.</p>

<p>A partners can leave the Financial Proxy Program but stay a member of the Community Partnership Program. In this case the same rules apply for announcing of the decision and spending of funds.</p>

<p>Costs for shutting down the Community Partnership Agreement or the Financial Proxy Program will be taken from the partner funds.</p>

<h2>Adjustment of program rules</h2>

<p>The Board of KDE e.V. is responsible for interpreting and adjusting the rules of the program where necessary. It will do that in the spirit of the goals of the program. The Board of KDE e.V. will notify all members of the Community Partnership Program of any changes to program rules at least four weeks in advance of becoming effective.</p>
