---
title: "KDE e.V. Announcements"
layout: page
nosubpage: true
---

Announcements can now be found in the
[news](/news) section,

<h3>2010</h3>

<ul>

<li><b>July 16th 2010:</b> <a href="/2010/07/16/akademy2010-concludes/">KDE's flagship conference Akademy Concludes: Pushing for Elegance and the Mobile Space</a></li>

<li><b>July 14th 2010:</b> <a href="/2010/07/14/kde-e-v-and-kde-espana-sign-an-agreement-officially-recognizing-kde-espana-as-oficial-representative-of-kde-e-v-in-spain/">KDE e.V. and KDE España Sign an Agreement Officially Recognizing KDE España As Oficial Representative of KDE e.V. in Spain</a></li>

<li><b>March 16th 2010:</b> <a href="/2010/03/16/gnome-foundation-and-kde-e-v-to-co-host-conferences-in-2011/">GNOME Foundation and KDE e.V. to Co-Host Conferences in 2011</a></li>

</ul>

<h3>2009</h3>

<ul>
<li><b>September 2nd 2009:</b> <a href="/2009/09/02/akademy-2010-in-tampere-finland/">Akademy 2010 in Tampere, Finland</a></li>
<li><b>August 6th 2009:</b> <a href="/2009/08/06/free-desktop-communities-come-together-at-the-gran-canaria-desktop-summit/">Free Desktop Communities come together at the Gran Canaria Desktop Summit</a></li>



</ul>

<h3>2008</h3>

<ul>

<li><b>August 22nd 2008:</b> <a href="/2008/08/22/fsfe-welcomes-kde-s-fiduciary-license-agreement/">FSFE welcomes KDE's Fiduciary License Agreement</a></li>
<li><b>July 11th 2008:</b> <a href="/2008/07/11/kde-and-gnome-to-co-locate-flagship-conferences-on-gran-canaria-in-2009/">KDE and
GNOME to Co-locate Flagship Conferences on Gran Canaria in 2009</a></li>
<li><b>April 22nd 2008:</b> <a href="/2008/04/22/kde-e-v-and-the-gnome-foundation-to-co-host-flagship-conferences/">KDE e.V. and the
GNOME Foundation to co-host flagship conferences</a></li>
<li><b>April 4th 2008:</b> <a href="/2008/04/04/kde-and-wikimedia-collaborate/">KDE and Wikimedia
Start Collaboration</a></li>
</ul>
