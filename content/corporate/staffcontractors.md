---
title: Staff and Contractors
menu:
  main:
    parent: organization
    weight: 2
staffcontractors:
  - name: Adam Szopa
    title: Project Coordinator (contractor)
    email: adam.szopa<span>@</span>kde.org
    description: Adam started working for KDE in 2020. Adam mainly helps coordinate work related to the KDE Goals, Akademy and other parts of the Community. He has a masters degree in computer science and likes to relax by playing video games. He lives in Poland.
    image: /corporate/pictures/adam.jpg
  - name: Aniqa Khokhar
    title: Marketing Consultant (contractor)
    email: aniqa.khokhar<span>@</span>kde.org
    description: Aniqa Khokhar is a Marketing Consultant and started working for KDE e.V. in 2020. She contributes to the KDE Promo team activities and strengthens marketing efforts for both the Community and the organization. She has experience in managing marketing campaigns, social media, events, customers, business planning, corporate communications, and market research in education and non-profit sectors. In her spare time, she loves cooking, gardening, and travelling.
    image: /corporate/pictures/aniqa.jpg
  - name: Dina Nouskali
    title: Event Organiser (contractor)
    email: dina.nouskali<span>@</span>kde.org
    description: Dina Nouskali is an Event Organiser and Digital Marketer, who started cooperating with KDE in 2022. She has a Bachelor in Marketing and Advertising and a Master in E-business and Digital Marketing and a big passion for Event Organising. During the last 10 years she has cooperated with different companies and organisations in organising conferences, cultural events, festivals and workshops.
    image: /corporate/pictures/unknown.png
  - name: Joseph P. De Veaugh-Geiss
    title: Project and Community Manager
    email: joseph<span>@</span>kde.org
    description: Joseph P. De Veaugh-Geiss has worked as project and community manager in the KDE Eco initiative since July 2021. The current project in this initiative is *Nachhaltige Software Für Nachhaltige Hardware* ('Sustainable Software For Sustainable Hardware'). Prior to working for KDE e.V. he completed a PhD in theoretical and experimental linguistics. When not advocating for free and environmentally-sustainable software, he enjoys learning about languages, listening to music and cooking, and spending as much time as possible under Berlin skies at Tempelhofer Feld.
    image: /corporate/pictures/joseph.jpg
  - name: Natalie Clarius
    title: Hardware Integrator (contractor)
    email: natalie.clarius<span>@</span>kde.org
    description: Natalie started working for KDE e.V. in 2023 as Hardware Integrator, supporting KDE's software with development and coordination to ensure that Plasma and apps work seamlessly on our vendor partners' hardware products. Her background is in computational linguistics and formal logic, which she is passionate about teaching. She has been an active KDE contributor since 2022, and works on various other software projects in her spare time.
    image: /corporate/pictures/unknown.png
  - name: Nicolas Fella
    title: Software Platform Engineer (contractor)
    email: nicolas.fella<span>@</span>kde.org
    description: Nicolas started contributing to KDE in 2017 and joined KDE e.V. as Software Platform Engineer in 2023. This means taking care of everything needed so that people can build and enjoy awesome software. When not hacking on KDE he enjoys cooking and biking.
    image: /corporate/pictures/unknown.png
  - name: Nicole Teale
    title: Project Lead and Event Manager
    email: nicole.teale<span>@</span>kde.org
    description: Nicole started at KDE in April 2024. She contributes to the KDE Eco project *Nachhaltige Software Für Nachhaltige Hardware* ('Sustainable Software For Sustainable Hardware'). In her spare time she likes all manner of crafts and craftiness, listening to music, and spending time with her family.
    image: /corporate/pictures/nicole.jpg
  - name: Paul Brown
    title: Marketing Consultant (contractor)
    email: paul.brown<span>@</span>kde.org
    description: Paul Brown started working as Marketing Consultant for KDE e.V. in 2017. He comes from the world of publishing and has been a Free Software advocate since 1996. He works with the members of the Promo team setting goals, managing campaigns and analysing results. He also helps KDE projects optimise their communication strategies and copywrites and proofreads their websites and blog posts. In his spare time, he enjoys 3D printing, writing tutorials on Free Software usage and articles on writing and communication. He also "enjoys" watching TV series and movies and then ranting about how the creators are "lazy writers" on Reddit and Twitter.
    image: /corporate/pictures/paul.png
  - name: Petra Gillert
    title: Assistant to the Board
    email: petra<span>@</span>kde.org
    description: Petra Gillert is the assistant of KDE e.V.'s board since 2015. She supports the board and the organisation in all matters of organisation and finance and manages the office in Berlin. She ensures continuity in the organisation. In her spare time she enjoys bird watching.
    image: /corporate/pictures/petra.png
  - name: Thiago Masato Costa Sueto
    title: Documentation Writer (contractor)
    email: thiago.sueto<span>@</span>kde.org
    description: Thiago joined KDE e.V. to support the multi-year documentation project, primarily as writer and documentation coordinator. He ensures that the onboarding documentation for new contributors is as accessible and painless as possible. In his free time, he enjoys studying languages, playing games, exercising, and experimenting with free software technologies.
    image: /corporate/pictures/unknown.png
---

KDE e.V. supports the KDE Community in various ways, one of them is through its staff and contractors to work on key areas.

## Current staff and contractors

{{< people-list name="staffcontractors" >}}

Interested in making a living with KDE? Check out the [open positions](/corporate/careers)!
