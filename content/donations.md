---
title: "Donations to the KDE e.V."
menu:
  main:
    parent: contact
    name: Donations
    weight: 3
---

## Support KDE e.V.

The KDE e.V. is a registered non-profit organization and accepted as
tax-exempt. Its [articles of association](/corporate/statutes/)
oblige to spend all donations it gets to support the KDE project. You can help 
the KDE project by donating to the KDE e.V. or by becoming a 
[Supporting Member](/supporting-members/).

> KDE e.V. does not accept donations of "crypto" coins: Bitcoin, Etherium,
> Litecoin, Dogecoin and all the others. The tax regime that covers
> German not-for-profit associations considers those coins a form
> of speculation which would endanger our status -- so regretfully
> you cannot donate them directly. Cash out and then use a traditional
> method to donate.

### One-time donations

For a one-time donation, 
the [donations page](https://kde.org/community/donations/)
can be used for any amount through a wide range of payment options.

### Recurring donations

If you want to support KDE by regularly donating money, consider becoming a 
Supporting Member. The [donations page](https://kde.org/community/donations/)
has additional information on your options.


## Individual Supporting Membership

You can become an individual Supporting Member of KDE e.V. through the Join the 
Game support program. Click on the image below to learn more about it!

<a href="https://jointhegame.kde.org"><img src="/images/Jtg.png" class="noborder" style="float: right; margin: 0px; background-image: none; border: 0;" alt="Join the Game and support KDE" /></a>

## Corporate Supporting Membership

Please see our [Supporting Membership information](/getinvolved/supporting-members/) 
page for more information.


## Questions

If you have any questions or are looking for more specific ways to support
KDE please contact the <a href="mailto:kde-ev-board@kde.org">board of the KDE
e.V.</a>

## Donations and taxes

As the KDE e.V. is a not-for-profit organisation (*Gemeinn&uuml;tziger Verein*), 
donations in Germany can be deducted from your taxes. Please refer
to [this page](/donations-taxes-de/) (in German) for more detailed information. 

### Receipts (Spendenquittungen)

If you need a receipt ("Spendenquittung") for your donation, please contact the
<a href="mailto:kde-ev-treasurer@kde.org">KDE e.V. treasurer</a>.
