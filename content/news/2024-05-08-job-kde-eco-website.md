---
title: 'KDE e.V. is looking for a web designer (Hugo) for environmental sustainability project'
date: 2024-05-08 17:30:00
layout: post
noquote: true
---

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a web designer who is skilled with [Hugo](https://gohugo.io/) to implement a new environmental sustainability campaign for the KDE Eco website. Please see the [job ad](/resources/jobad-KDE-Eco-Website_2024.pdf) for more details about this employment opportunity.

We are looking forward to your application.
