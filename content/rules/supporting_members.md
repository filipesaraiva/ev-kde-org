---
title: "Rules of Procedures for Supporting Members"
layout: page
---

<p>The following two documents are the text of the rules of procedure for the
supporting membership of the KDE e.V. The content of the two versions is the
same, but the German version is the legally binding one.</p>

<p><a href="/rules/supporting_members.pdf">Rules of Procedure Supporting Members
(English Version)</a></p>
<p><a href="/rules/supporting_members_german.pdf">Rules of Procedure Supporting Members
(German Version)</a></p>

<p><em>The rules of procedure for supporting members have been decided by the
membership of the KDE e.V. through an online vote on February 22th 2006.</em></p>
