<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Supported_Activities/Linux_Application_EcoSystem_c.jpg" alt="Attendees to the Linux Application Ecosystem event" />
    </figure>
</div>

I helped KDE Network China and Ubuntu Kylin's with its first event, Linux Application Ecosystem Salon on October 23rd at Central South University, Changsha. The goal of this event was to promote the development of FOSS/FLOSS software within Chinese universities and to make it easier for students to understand the development of open source in China by popularizing KDE software and open-source activities in other communities.

We had around 10 speakers and over 100 students who participated. We had sessions about KDE and the GNU/Linux Real World and KDE Plasma Mobile.

After the event, I was approached by many students asking about how can they support KDE.

As an organizer and participant of this event, I went on my first outreach trip on behalf of KDE. Through my interactions with the students, I learned that the students were interested in participating in FOSS, either out of interest or in terms of career planning. Perhaps this is the beauty of the FOSS culture that inspires us.
