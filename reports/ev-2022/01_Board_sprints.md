<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Board/2022winterboardsprint_cropped.jpg" alt="Board mambers in Berlin, from left to right: Adriaan DE Groot, Eikie Hein, Aleix Pol, Nate Graham, Lydia Pintscher, all looking quite cold... except for Nate" />
    </figure>
</div>

The Board members of KDE e.V. meet weekly online, but they also have in-person board meetings and sprints. In 2022, the board met twice in person for the Board Sprints.

<figure class="image-right"> <img width="100%" src="images/Sprints/Board/board2.webp" alt="Board sprint pre-2022 elections, with Neofytos Kolokotronis." /> </figure>

The first Board Sprint was held from 4th to 5th June 2022 in Athens, Greece. It was a hybrid meeting, with Adriaan de Groot, Aleix Pol i Gonzàlez, Lydia Pintscher and Neofytos Kolokotronis physically present and Eike Hein participating virtually.

The second one was held on the 10th and 11th December 2022 in Berlin, Germany. All the Board members, Adriaan de Groot, Aleix Pol i Gonzàlez, Eike Hein, Lydia Pintscher and Nate Graham, met in person.

The agendas of these board meetings focus on various important strategic, operational and administrative topics such as HR, budgeting, and patron relations. These sprints are a chance for the Board members to come together, discuss important matters, and socialize in person, which helps to create a sense of community and enhance the overall functioning of KDE.

Overall, these sprints are productive weekends, where many tasks are solved and new ones are started.
