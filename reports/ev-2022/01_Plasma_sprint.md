From the 5th to the 6th of March, the Plasma team held the [Plasma 6 sprint](https://invent.kde.org/plasma/plasma-workspace/-/issues/32), which covered the migration to Qt6/KF6.

In general, the migration strategy is similar for Plasma as it is for Frameworks: do as much as possible in the 5 codebases in incremental steps. It’s much easier to test changes on a working foundation, we can deliver improvements to our users in the usual cycle for longer, and we save extra merging/backporting work. Quite a few things were identified that can be done right away based on Qt5/KF5, such as:

* Port away from `Plasma::DataEngine`, which is being phased out in favor of "normal" QML modules.
* Complete the port away from Plasma Components 2 (PC2), which are still using the deprecated Qt Quick Controls 1.
* Complete the migration to Plasma’s KWaylandServer module, which supersedes the corresponding Frameworks API.
* Port away from Qt or KF API that has been deprecated and replaced for the latest 5 release already.

You can find the full list on the [Plasma 6 workboard](https://phabricator.kde.org/tag/plasma_6/).

Another important task is to clearly mark obsolete code and features as such, ie. things that are scheduled for removal in 6, so we don’t waste any time on porting those, and instead focus on migrating remaining uses away from them.
