[QtCon Brasil](https://br.qtcon.org/) was held online from 21st to 26th November 2022. QtCon Brazil is a space where Brazilians and Latin Americans can meet to do business and exchange experiences related to [Qt](https://www.qt.io/) technology. 

KDE was the silver sponsor of the event, and KDE developers Tomaz Canabravaand Nicolas Fella gave talks related to KDE.

Tomaz Canabrava gave a talk on "Konsole: Keeping the Terminal Relevant in the Age of GUIs". This talk discussed how Qt has helped develop Konsole, one of the most widely used terminals in the world, on Unix, and what aspects Konsole has implemented to stay relevant in a world where the use of graphical tools is constantly increasing.

Nicolas Fella gave a Keynote on "KDE’s journey to Qt 6". The KDE Community has been developing software with Qt for 25 years. Some of the code has been ported across all major versions of Qt. With Qt 6 being released, the next major version transition is coming. This presents both a challenge and an opportunity for the community.

This talk presented the work that has gone into, and is planned for the Qt 6 transition, the challenges we face and the design decisions that need making. It highlighted which aspects of the Qt 6 transition are going to be difficult and how new features of Qt 6 are going to benefit KDE. Attendees were able to use KDE’s experience to plan and estimate the workload to port their own projects.
